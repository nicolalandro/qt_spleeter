import sys
from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtWidgets import QFileDialog
import os
import subprocess

# env variables
SPLEETER_VERSION = os.getenv('SPLEETER_VERSION')
if SPLEETER_VERSION is None:
    SPLEETER_VERSION =  "1.5.0" 
    
SPLEETER_COMMAND = os.getenv('SPLEETER_COMMAND')
if SPLEETER_COMMAND is None:
    SPLEETER_COMMAND =  "spleeter"

qtcreator_file = "mainwindow.ui"  # Enter file here.
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtcreator_file)


class MyWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.input_file_button.clicked.connect(self.select_input_file)
        self.output_folder_button.clicked.connect(self.select_output_folder)
        self.spleeter_button.clicked.connect(self.spleeter)
        self.progressBar.setVisible(False)

    def select_input_file(self):
        fileName, _ = QFileDialog.getOpenFileName(
            self, "QFileDialog.getOpenFileName()", "", "Audio Files (*.mp3 *.wav)")
        if fileName:
            print('mp3 file path:', fileName)
            self.input_file_box.setText(fileName)

    def select_output_folder(self):
        dirName = QFileDialog.getExistingDirectory(self, "Select Directory")
        if dirName:
            print('output dir path', dirName)
            self.output_file_box.setText(dirName)

    def spleeter(self):
        print('spleeter')
        mode = 'spleeter:2stems'
        bindex = self.comboBox.currentIndex()
        if bindex == 1:
            mode = 'spleeter:4stems'
        elif bindex == 2:
            mode = 'spleeter:5stems'

        command = SPLEETER_COMMAND.split(' ') + [
            'separate',
            '-p',
            mode,
            '-o',
            f'{str(self.output_file_box.text())}',
            '-i',
            f'{str(self.input_file_box.text())}',
        ]

        v = [int(x) for x in SPLEETER_VERSION.split('.')]
        if v[0] > 2 or (v[0] == 2 and v[1] >= 1):
            command.remove('-i')

        command_str = " ".join(command)
        print(command_str)

        self.Logs.setPlainText(command_str)
        self.Logs.repaint()
        self.progressBar.setVisible(True)
        self.progressBar.repaint()
        
        proc = subprocess.Popen(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # this should be async
        while True:
            if proc.stdout is not None:
                line = proc.stdout.readline()
                rline = line.rstrip().decode('utf-8')
            else:
                rline = ""
            if proc.stderr is not None:
                stderr_line = proc.stderr.readline()
                rerr = stderr_line.rstrip().decode('utf-8')
            else:
                rerr = ""
            if rline == "" and rerr == "":
                break
            self.Logs.setPlainText(
                self.Logs.toPlainText() + '\nLog:' + rline + '\nErr:' + rerr)
            self.Logs.repaint()
        self.progressBar.setVisible(False)



if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MyWindow()
    window.show()
    sys.exit(app.exec_())
